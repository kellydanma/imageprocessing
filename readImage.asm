.data
p:		.asciiz "P"
two:		.asciiz "2"
five:		.asciiz "5"
ws: 		.asciiz " "	# whitespace
nl:		.asciiz "\n"	# newline
errInvalidFile:	.asciiz "error in readImage.asm: invalid .pgm file\n"
buffer:		.space	4096	# buffer to read from file
width:		.word 1
height:		.word 1
maxval:		.word 1
.text
.globl read_image

read_image:
	# $a0 -> input file name
	################# return #####################
	# $v0 -> Image struct :
	# struct image {
	#	int width;
	#       int height;
	#	int max_value;
	#	char contents[width*height];
	#	}
	##############################################
	
	li   	$v0, 13		# syscall for open file
	li   	$a1, 0        	# flag for reading
	syscall            	# open file 
	beq 	$v0, -1, err 	# file opening error
	move 	$t0, $v0      	# save file descriptor in t0
	li   	$v0, 14       	# syscall for reading file
	move 	$a0, $t0      	# a0 = file descriptor 
	la   	$a1, buffer   	# a1 = addr of buffer
	li   	$a2, 2048  	# a2 = hardcoded buffer length
	syscall            	# read from file
	beq 	$v0, -1, err 	# file reading error
	la	$a1, buffer	# a1 = addr of buffer
	move	$t9, $ra	# save the addr of $ra for later
	
ptype:	# verify that the file starts with "P"
	lbu	$t1, 0($a1)	# t1 = a1[i]
	addi	$a1, $a1, 1	# a1++
	la	$t2, ws
	lbu	$t2, 0($t2)	# t2 = " "
	la	$t3, nl
	lbu	$t3, 0($t3)	# t3 = "\n"
	beq 	$t1, $t2, ptype	# whitespace, check next byte
	beq	$t1, $t3, ptype	# newline, check next byte
	beq	$t1, 0, err	# end of file error
	la	$t2, p
	lbu	$t2, 0($t2)	# t2 = "P"
	bne	$t1, $t2, err	# not a .pgm file, doesn't start with "P"
	
	# verify that the next integer is either 2 or 5
	move	$a0, $zero	# a0 = 0 (which represents P2 file)
	lbu	$t1, 0($a1)
	addi	$a1, $a1, 1	# a1++
	la	$t2, two
	lbu	$t2, 0($t2)	# t2 = "2"
	beq	$t1, $t2, dim	# valid .pgm file, P2, goto dim (to find dimensions)
	addiu	$a0, $a0, 1	# a0 = 1 (which represents P5 file)
	la	$t2, five
	lbu	$t2, 0($t2)	# t2 = "5"
	beq	$t1, $t2, dim	# valid .pgm file, P5, goto dim (to find dimensions)
	j	err		# not a .pgm file, invalid integer after "P"
	
dim:	jal	start		# find width
	sw	$t4, width	# store width
	move	$t6, $t4	# t6 = width
	jal	start		# find height
	sw	$t4, height	# store height
	move	$t7, $t4	# t7 = width
	jal	start		# find max value
	sw	$t4, maxval	# store max value

	# dynamically allocate memory for Image Struct
	mul 	$t1, $t6, $t7	# $t1 = width * height
	addiu	$t1, $t1, 12	# t1 += 12 
	move	$t5, $a0
	move 	$a0, $t1	# allocate ((width * height) + 12) bytes	
	li 	$v0, 9		# syscall for malloc
	syscall			# dynamically allocate space for Image struct
	add	$a3, $v0, $t1	# a3 = addr of the byte after the last one in struct
	la	$t0, width
	lw	$t0, 0($t0)
	sw	$t0, 0($v0)	# save width into Image struct
	la	$t0, height
	lw	$t0, 0($t0)
	sw	$t0, 4($v0)	# save height into Image struct
	la	$t0, maxval
	lw	$t0, 0($t0)
	sw	$t0, 8($v0)	# save max value into Image struct
	move	$v1, $v0	# v1 = v0
	addiu	$v0, $v0, 12	# v0 + 12
	move	$a0, $t5
	bne	$a0, $zero, p5	# if a0 = 1 goto p5 (for P5 files)
	
p2:	# add pixels to Image struct for a P2 file
	beq	$v0, $a3, end	# end of Image struct
	jal	start		# find pixel value, stored in $t4
	sb	$t4, 0($v0)	# store least significant 8 bits of $t4 into addr v0
	addiu	$v0, $v0, 1	# v0++
	j	p2		# find next pixel, loop
	
p5:	# add pixels to Image struct for a P5 file
	beq	$v0, $a3, end	# end of Image struct
	move	$t1, $zero 	# reset t1
	lbu	$t1, 0($a1)	# load next byte
	sb	$t1, 0($v0)	# store current byte 
	addiu	$v0, $v0, 1	# v0++
	addiu	$a1, $a1, 1	# a1++
	j	p5		# find next pixel, loop
	
end:	move	$v0, $v1	# return the address of Image struct
	move	$ra, $t9	# restore value of $ra
	jr 	$ra

start:	la	$t2, ws
	lbu	$t2, 0($t2)	# t2 = " "
	la	$t3, nl
	lbu	$t3, 0($t3)	# t3 = "\n"
	lbu	$t1, 0($a1)	# t1 = a1[i]
	addiu	$a1, $a1, 1	# a1++
	beq 	$t1, $t2, start	# whitespace, check next byte
	beq	$t1, $t3, start	# newline, check next byte
	
	# start of new value, t4 = 0
	add	$t4, $zero, $zero
	addiu	$t5, $zero, 10	# t5 = 10
	addiu	$a1, $a1, -1	# a1--
	
loop:	mul	$t4, $t4, $t5
	addiu	$t1, $t1, -48
	add	$t4, $t4, $t1
	addi	$a1, $a1, 1	# a1++
	lbu	$t1, 0($a1)	# t1 = a1[i]
	beq 	$t1, $t2, save	# whitespace, save value
	beq	$t1, $t3, save	# newline, save value
	j	loop		# find next digit
	
save:	# the new value is in $t4
	addiu	$a1, $a1, 1	# a1++
	jr	$ra
	
err:	# invalid .pgm file error
	# all types of errors are abstracted
	li 	$v0, 4 
	la 	$a0, errInvalidFile
	syscall 
	li 	$v0, 10		# exit...
	syscall	