.data
errInvalidLoc:	.asciiz "error: invalid row or column number\n"
errInvalidPix:	.asciiz "error: pixel too large, setting instead to 255\n"
errInvalidNeg:	.asciiz "error: negative pixel, setting instead to 0\n"
pixel:		.word 1	
.text
.globl set_pixel
set_pixel:
	# $a0 -> image struct
	# $a1 -> row number
	# $a2 -> column number
	# $a3 -> new value (clipped at 255)
	###############return################
	#void
	
	# verify that row and col nr are within range
	lw	$t0, 0($a0)	# t0 = width
	bge 	$a2, $t0, errLoc# if col nr >= width, goto err
	lw	$t1, 4($a0)	# t1 = height
	bge 	$a1, $t1, errLoc# if row nr >= height, goto err
	la	$t3, 8($a0)	# t3  = address of max val
	lw	$t4, 0($t3)	# t4 = maxval
	
	# set pixel if no errors have occurred
	la	$t2, 12($a0)	# t2 = addr of array at (0, 0)
	mult	$t0, $a1	# width * row
	mflo	$t0
	add	$t2, $t2, $t0	# t2 += rows
	add	$a0, $t2, $a2	# a0 = t2 col nr
	bltz	$a3, zero	# if t0 < 0 goto zero
	li	$t0, 255
	bgt	$a3, $t0, errPix# if t0 > 255 goto errPix
	sb	$a3, 0($a0)	# store into pixel
	blt	$a3, $t4, fin	# if pixel < max value goto fin
	sb	$a3, 0($t3)	# else, store new max value
fin:	jr 	$ra

zero:	# stores the value 0
	sb	$zero, 0($a0)
	li 	$v0, 4 
	la 	$a0, errInvalidNeg
	syscall 
	j	fin

errPix:	# prints error & sets pixel value to 255
	li	$t0, 255	# t0 = 255
	sb	$t0, pixel
	lbu	$t0, pixel
	sb	$t0, 0($a0)
	li 	$v0, 4 
	la 	$a0, errInvalidPix
	syscall 
	j	fin		# goto fin

errLoc:	# prints error
	li 	$v0, 4 
	la 	$a0, errInvalidLoc
	syscall 
	j	fin		# goto fin
