.data
maxval:		.word 1
minval:		.word 1
.text
.globl rescale_image
rescale_image:
	# $a0 -> image struct
	############return###########
	# $v0 -> rescaled image
	######################
	
	move	$t9, $ra	# save $ra for later
	move	$v0, $a0	# v0 = a0
	lw	$t0, 8($v0)	# t0 = max value
	sw	$t0, maxval	# store max value
	sw	$t0, minval	# store max value at minval for now
	jal	findmin		# find & store min value
	li	$t0, 255	# t0 = 255
	sw	$t0, 8($v0)	# max value is 255 now
	lw	$t0, maxval	# t0 = maxval
	lw	$t1, minval	# t1 = minval
	beq	$t0, $t1, fin	# pixels do not change, only 1 value
	la	$a0, 12($v0)	# a0 = addr of start of array
	move	$a1, $t1	# a1 = minval
	sub	$a2, $t0, $a1	# a2 = maxval - minval
	lw	$t1, 0($v0)	# t1 = width
	lw	$t2, 4($v0)	# t2 = height
	mult	$t2, $t1	# total = width * height
	mflo	$t1		# t1 = length of array
	add	$a3, $a0, $t1	# a3 = addr of first byte that is > array
	jal	rescale		# rescale image
	
fin:	move	$ra, $t9	# restore ra value
	jr 	$ra
	
rescale:# new value = ((x - min) * 255) / (max - min)
	# a0 = array index
	# a1 = minval
	# a2 = maxval - minval
	# a3 = addr of first byte that is > array

	bge	$a0, $a3, rfin	# iterated through array, goto rfin
	lbu	$t0, 0($a0)	# load next byte
	sub	$t0, $t0, $a1	# t0 -= minval
	li	$t1, 255	# t1 = 255
	mult	$t0, $t1	# t0 * 255 (max 5 bits)
	mflo	$t0		# t0 = ((x - min) * 255)
	mtc1 	$t0, $f1	# f1 = single-precision float of t0
  	cvt.s.w $f1, $f1	# convert from word to single precision
	mtc1 	$a2, $f2	# f2 = single-precision float of a2
  	cvt.s.w $f2, $f2	# convert from word to single precision
	div.s 	$f3, $f1, $f2	# f3 = f1 / f2
	round.w.s $f3, $f3	# round the floating point number
	mfc1 	$t0, $f3      	# move rounded integer into t0
	sb	$t0, 0($a0)	# store in Image struct
	addiu	$a0, $a0, 1	# increase a0
	j	rescale

rfin:	jr	$ra

findmin:# find min value in Image struct
	la	$a0, 12($v0)	# a0 = addr of start of array
	lw	$t1, 0($v0)	# t1 = width
	lw	$t2, 4($v0)	# t2 = height
	mult	$t2, $t1	# total = width * height
	mflo	$t1		# t1 = length of array
	lw	$t0, 8($v0)	# t0 = max value
	move	$a1, $t0	# a1 = max value
	add	$a2, $a0, $t1	# a2 = addr of first byte that is > array
	
mloop:	bge	$a0, $a2, mfin	# iterated through array, goto fin
	lbu	$t0, 0($a0)	# load next byte
	blt	$t0, $a1, nmin	# new min value
	addiu	$a0, $a0, 1	# increase a0
	j	mloop
	
nmin:	# update the new min
	move	$a1, $t0	# t0 = new min value in a1
	addiu	$a0, $a0, 1	# increase a0
	j	mloop
	
mfin:	sw	$a1, minval	# store min value
	jr 	$ra
