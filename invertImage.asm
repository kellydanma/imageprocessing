.data
maxval:	.word 1
newmax: .word 1
.text
.globl invert_image
invert_image:
	# $a0 -> image struct
	#############return###############
	# $v0 -> new inverted image
	############################
	
	move	$v0, $a0	# v0 will be the inverted Image struct
	move	$v1, $ra	# store $ra
	la	$a0, 12($v0)	# a0 = addr of start of array
	lw	$a1, 8($v0)	# a1 = max value
	sw	$a1, maxval	# store a1 at maxval
	sw	$zero, newmax	# set max value at zero to start with
	lw	$t1, 0($v0)	# t1 = width
	lw	$t2, 4($v0)	# t2 = height
	mult	$t2, $t1	# total = width * height
	mflo	$t1		# t1 = length of array
	add	$a2, $a0, $t1	# a2 = addr of first byte that is > array
	
loop:	bge	$a0, $a2, fin	# iterated through array, goto fin
	lbu	$t0, 0($a0)	# load byte
	lbu	$t1, maxval	# load maxval
	sub	$t0, $t1, $t0	# t0 = max val - byte 
	sb	$t0, 0($a0)	# store inverted value
	lbu	$t2, newmax	# t2 = new max value
	bgt	$t0, $t2, nmax	# new max value
	addiu	$a0, $a0, 1	# increase a0
	j	loop
	
fin:	lw	$t0, newmax	# load new max
	sw	$t0, 8($v0)	# store new max value
	move	$ra, $v1	# retrieve $ra value
	jr 	$ra

nmax:	# update the new max
	sw	$t0, newmax	# store new max value
	addiu	$a0, $a0, 1	# increase a0
	j	loop
