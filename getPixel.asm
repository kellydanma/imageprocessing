.data
errInvalidLoc:		.asciiz "error: invalid row or column number\nv0 = 0\n"
pixel:			.word 	1
.text
.globl get_pixel
get_pixel:
	# $a0 -> image struct
	# $a1 -> row number
	# $a2 -> column number
	################return##################
	# $v0 -> value of image at (row,column)
	#######################################
	
	# verify that row and col nr are within range
	lw	$t0, 0($a0)	# t0 = width
	bge 	$a2, $t0, err	# if col nr >= width, goto err
	lw	$t1, 4($a0)	# t1 = height
	bge 	$a1, $t1, err	# if row nr >= height, goto err
	
	# retrieve pixel if no errors have occurred
	la	$t2, 12($a0)	# t2 = addr of array at (0, 0)
	mult	$t0, $a1	# width * row
	mflo	$t0
	add	$t2, $t2, $t0	# t2 += rows
	add	$t2, $t2, $a2	# t2 += col nr
	sw	$zero, pixel	# clear pixel
	lbu	$t0, 0($t2)	# t0 = pixel value
	sb	$t0, pixel	# store into pixel
	lw	$v0, pixel	# store byte at t2 into pixel
	
fin:	jr 	$ra

err:	# prints error and returns 0
	li 	$v0, 4 
	la 	$a0, errInvalidLoc
	syscall 
	move	$v0, $zero	# v0 = 0 if error occurs
	j	fin		# goto fin
