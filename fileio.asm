.data
			.align 2
inputTest1:		.asciiz "images/test1.txt"
			.align 2
inputTest2:		.asciiz "images/test2.txt"
			.align 2
outputFile:		.asciiz "images/copy.pgm"
			.align 2
writeData:		.asciiz "P2\n24 7\n15\n"
buffer:			.space 1024
errOpeningFile:		.asciiz "error opening file\n"
errClosingFile:		.asciiz "error closing file\n"
errReadingFile:		.asciiz "error reading file\n"
errWritingFile:		.asciiz "error writing file\n"
.text
.globl fileio

fileio:
	la 	$a0, inputTest1
	#la 	$a0, inputTest2 # this is not actually an image
	jal 	read_file
	
	la 	$a0, outputFile
	jal 	write_file
	
exit:	li 	$v0, 10		# exit...
	syscall	
		
read_file:
	# $a0 -> input filename	
	# Opens file
	# read file into buffer
	# return
							
	li   	$v0, 13		# syscall for open file
	la   	$a0, 0($a0)   	# load input file name
	li   	$a1, 0        	# flag for reading
	syscall            	# open file 
	beq 	$v0, -1, oerr 	# file opening error
	move 	$t0, $v0      	# save file descriptor in t0
	
	li   	$v0, 14       	# syscall for reading file
	move 	$a0, $t0      	# a0 = file descriptor 
	la   	$a1, buffer   	# a1 = addr of buffer
	li   	$a2, 1024  	# a2 = hardcoded buffer length
	syscall            	# read from file
	beq 	$v0, -1, rerr 	# file reading error
	
	li  	$v0, 4   	# syscall for printing to stdout	
	la  	$a0, buffer	# buffer contains the values to be printed
	syscall             	# print 
	
	li   	$v0, 16       	# syscall for closing a file
	move 	$a0, $t0      	# file descriptor to close
	syscall            	# close file
	beq 	$v0, -1, cerr 	# file closing error
	
	jr 	$ra
	
write_file:
	# $a0 -> outputFilename
	# open file for writing
	# write following contents:
	# P2
	# 24 7
	# 15
	# write out contents read into buffer
	# close file
	
	li   	$v0, 13		# syscall for open file
	la   	$a0, 0($a0)   	# load input file name
	li   	$a1, 1        	# flag for writing
	syscall            	# opens file 
	beq 	$v0, -1, oerr 	# file opening error
	move 	$t0, $v0      	# save file descriptor in t0
	
	li 	$v0, 15 	# syscall for writing to file
	move 	$a0, $t0 	# a0 = file descriptor
	la	$a1,writeData	# a1 = addr of writeData
	li 	$a2, 11 	# amount to write 
	syscall 		# write to file
	beq 	$v0, -1, werr  	# file writing error
	
	li 	$v0, 15 	# syscall for writing to file
	move 	$a0, $t0 	# a0 = file descriptor
	la 	$a1, buffer 	# a1 = addr of buffer 
	li 	$a2, 1024 	# amount to write 
	syscall 		# write to file
	beq 	$v0, -1, werr  	# file writing error
	
	li   	$v0, 16       	# syscall for closing a file
	move 	$a0, $t0      	# file descriptor to close
	syscall            	# close file
	beq 	$v0, -1, cerr 	# file closing error

	jr 	$ra
	
oerr: 	# file opening error 
	li 	$v0, 4 
	la 	$a0, errOpeningFile
	syscall 
	j 	exit

cerr: 	# file closing error 
	li 	$v0, 4 
	la 	$a0, errClosingFile
	syscall 
	j 	exit

rerr: 	# file reading error 
	li 	$v0, 4 
	la 	$a0, errReadingFile
	syscall 
	j 	exit

werr: 	# file writing error 
	li 	$v0, 4 
	la 	$a0, errWritingFile
	syscall 
	j 	exit
