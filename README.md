# ImageProcessing

Simple image processing toolbox for GIMP files written in assembly.

The GIMP documentation can be found [here](http://netpbm.sourceforge.net/doc/pgm.html).

The specifications for proper `.pgm` file formatting are especially relevant to this project.

Download GIMP [here](https://www.gimp.org/).

## Image struct

The image struct used is represented as such:

```C
struct image {
    int width;
    int height;
    int max_value;
    char contents[width*height];
}
```

The 2D `contents` array will be represented in 1D in assembly.

## Functions

The global functions are tested in `main.asm` while output `.pgm` files are located in the `images` subdirectory.

The first row is 0, the first column is 0.

The rescaling function, `rescale_image`, scales an existing image to use the max value of `255`.

| Function | Parameters | Output | Description |
|:-:|:-:|:-:|---|
| `get_pixel` | `$a0 -> image struct`, `$a1 -> row number`, `$a2 -> column number`| `$v0 -> value of image at (row, column)` | Returns the pixel as a `byte` (8 bits) at (row, column) |
| `set_pixel` |  `$a0 -> image struct`, `$a1 -> row number`, `$a2 -> column number`, `$a3 -> new value (clipped at 255)` | `void`  | Sets a new pixel value at (row, column); the value must be within the range [0, 255] |
| `read_image` | `$a0 -> input file name` | `$v0 -> Image struct`  | Returns an `image struct` representation of a file  |
| `write_image` | `$a0 -> image struct`, `$a1 -> output filename`, `$a2 -> .pgm file type` |  `void` | Writes an image struct to file; the file is a P5 file if `$a2` is 0, P2 if `$a2` is 1 |
| `invert_image` | `$a0 -> image struct`  | `$v0 -> new inverted image`  | Returns an image struct with inverted pixel values  |
| `rescale_image` | `$a0 -> image struct`  | `$v0 -> rescaled image`  | Returns a rescaled image where each `new pixel = ((x - min) * 255) / (max - min)`  |
