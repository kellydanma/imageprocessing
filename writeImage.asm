.data
p2:		.asciiz "P2 "
p5:		.asciiz "P5 "
newline:	.asciiz "\n"
whitespace:	.asciiz " "
errInvalidFile:	.asciiz "error in writeImage.asm: error writing to file\n"
width:   	.space 10
height:		.space 10
maxval:		.space 10
pixel:		.word  1

.text
.globl write_image
write_image:
	# $a0 -> image struct
	# $a1 -> output filename
	# $a2 -> type (0 -> P5, 1->P2)
	################# returns #################
	# void
	# Add code here.
	
	move	$s1, $ra	# store value of $ra for later
	move	$s2, $a0	# s2 = addr of Image struct
	move	$s3, $a1	# s3 = output file name
	move	$s4, $a2	# s4 = file type (P5 or P2)
	
	li   	$v0, 13		# syscall for open file
	move   	$a0, $s3   	# load output file name
	li   	$a1, 1        	# flag for writing without append
	syscall            	# opens file 
	beq 	$v0, -1, err 	# file opening error
	move 	$s5, $v0      	# save file descriptor in s5
	jal	ptype		# write file type to file
	li   	$v0, 16       	# syscall for closing a file
	move 	$a0, $s5      	# file descriptor to close
	syscall            	# close file
	beq 	$v0, -1, err 	# file closing error
	li   	$v0, 13		# syscall for reopening file
	move   	$a0, $s3   	# load output file name
	li   	$a1, 9        	# flag for writing WITH append
	syscall            	# opens file 
	beq 	$v0, -1, err 	# file opening error
	
	# print width (ASCII)
	lw	$a0, 0($s2)	# a0 = width
	jal	nrbytes		# find nr of bytes to print
	li   	$t1, 10  	# t1 = 10
	div  	$a0, $t1  	# perform division: a0 / 10
	mflo 	$t1  		# a0 / 10 (quotient)
	la	$a1, width	# address to store width
	jal	int2str		# write width to file
	li 	$v0, 15 	# syscall for writing to file
	move 	$a0, $s5 	# s5 = file descriptor	
	la	$a1, width	# a1 = addr of width
	syscall
	beq 	$v0, -1, err  	# file writing error
	jal	prints		# print whitespace
	
	# print height (ASCII)
	lw	$a0, 4($s2)	# a0 = height
	jal	nrbytes		# find nr of bytes to print
	li   	$t1, 10  	# t1 = 10
	div  	$a0, $t1  	# perform division: a0 / 10
	mflo 	$t1  		# a0 / 10 (quotient)
	la	$a1, height	# address to store height
	jal	int2str		# write height to file
	li 	$v0, 15 	# syscall for writing to file
	move 	$a0, $s5 	# s5 = file descriptor	
	la	$a1, height	# a1 = addr of height
	syscall 		# write to file
	beq 	$v0, -1, err  	# file writing error
	jal	prints		# print whitespace
	
	# print maxvalue (ASCII)
	lw	$a0, 8($s2)	# a0 = maxval
	jal	nrbytes		# find nr of bytes to print
	li   	$t1, 10  	# t1 = 10
	div  	$a0, $t1  	# perform division: a0 / 10
	mflo 	$t1  		# a0 / 10 (quotient)
	la	$a1, maxval	# address to store maxval
	jal	int2str		# write maxval to file
	li 	$v0, 15 	# syscall for writing to file
	move 	$a0, $s5 	# s5 = file descriptor	
	la	$a1, maxval	# a1 = addr of maxval
	syscall 		# write to file
	beq 	$v0, -1, err  	# file writing error
	jal	printl		# print a newline
	
	# calculate length of 1D array & print array
	lw	$s6, 0($s2)	# s6 = width
	lw	$t0, 4($s2)	# t1 = height
	mult	$t0, $s6	# multiply
	mflo	$s7		# the LSBs from multiplication
	move	$a0, $zero	# a0 = 0 (keep track of total)
	move	$a1, $zero	# a1 = 0 (keep track of columns)
	la	$a2, 12($s2)	# a2 = start of array bytes in Image struct
	beqz	$s4, p5write	# if P5 file, write bytes
	jal	p2write		# else, write ASCII for a P2 file
finish:	li   	$v0, 16       	# syscall for closing a file
	move 	$a0, $s5      	# file descriptor to close
	syscall            	# close file
	beq 	$v0, -1, err 	# file closing error
	move	$ra, $s1	# restore value of $ra
	jr 	$ra
	
p5write:# this is used to write a P5 file
	move	$t5, $a0	# t5 = total
	move	$t7, $a2	# t7 = address in Image struct
	beq	$t5, $s7, p5fin	# end of array
	li 	$v0, 15 	# syscall for writing to file
	move 	$a0, $s5 	# s5 = file descriptor	
	la	$a1, 0($t7)	# a1 = addr of pixel in struct
	li	$a2, 1		# write amount is 1 byte
	syscall 		# write to file
	beq 	$v0, -1, err  	# file writing error
	addiu	$a0, $t5, 1	# t0++
	addiu	$a2, $t7, 1	# t2++
	j	p5write
	
p5fin:	j	finish
	
p2write:# this is used to write a P2 file
	move	$t5, $a0	# save values
	move	$t6, $a1
	move	$t7, $a2
	beq	$t5, $s7, p2fin	# end of array
	sw	$zero, pixel	# clear pixel
	lbu	$t3, 0($t7)	# load byte value
	or	$t3, $t3, $zero	# or with zero to make it 32-bit
	move	$a0, $t3	# convert a0
	jal	nrbytes		# find nr of bytes to print, stored in $a2
	la	$a1, pixel	# address to store pixel
	jal	int2str		# write pixel to file
	li 	$v0, 15 	# syscall for writing to file
	move 	$a0, $s5 	# s5 = file descriptor	
	la	$a1, pixel	# a1 = addr of pixel
	#li 	$a2, 4 		# amount to write 
	syscall 		# write to file
	beq 	$v0, -1, err  	# file writing error
	
	# increment counters
	addiu	$t4, $s6, -1	# t4 = width - 1
	beq	$t4, $t6, newl2	# print a newline
	jal	prints
	addiu	$a0, $t5, 1	# t0++
	addiu	$a1, $t6, 1	# t1++
	addiu	$a2, $t7, 1	# t2++
	j	p2write		# loop

p2fin:	j	finish

newl2:	# prints newline and resets counter
	addiu	$t4, $s7, -1	# t4 = total - 1
	beq	$t5, $t4, skip	# skip printing a newline
	li 	$v0, 15 	# syscall for writing to file
	move 	$a0, $s5 	# s5 = file descriptor	
	la	$a1, newline	# a1 = addr of newline
	li	$a2, 1		# write amount is 1 byte
	syscall 		# write to file
	beq 	$v0, -1, err  	# file writing error
skip:	addiu	$a0, $t5, 1	# t0++
	move	$a1, $zero	# reset a1
	addiu	$a2, $t7, 1	# t2++
	j	p2write

ptype:	# write the file type
	li 	$v0, 15 	# syscall for writing to file
	move 	$a0, $s5 	# s5 = file descriptor
	beq 	$s4, $zero, f5
	la	$a1, p2		# a1 = addr of p2
	j	wtype		# goto wtype
f5:	la	$a1, p5		# a1 = addr of p5	
wtype:	li 	$a2, 3 		# write amount is 3 bytes 
	syscall 		# write to file
	beq 	$v0, -1, err  	# file writing error
	jr	$ra

int2str:# converts an integer to a char array
	# $a0 = integer to convert
	# $a1 = address of string where converted number is stored
	addi 	$sp, $sp, -4	# save space on stack pointer
	lw	$zero, 0($sp)
	sw   	$t0, 0($sp)  	# store addr of sp in t0
	li   	$t0, -1
	addi 	$sp, $sp, -4 	# allocate space on stack
	sw   	$t0, 0($sp) 	# save -1 on stack

push:	blez 	$a0, next1 	# if a0 < 0 goto next1
	li   	$t0, 10  	# t0 = 10
	div  	$a0, $t0  	# perform division: a0 / 10
	mfhi 	$t0      	# a0 % 10 (remainder)
	mflo 	$a0  		# a0 / 10 (quotient)
	addi 	$sp, $sp, -4	# allocate space on stack
	sw   	$t0, 0($sp) 	# store remainder on stack
	j    	push          	

next1:	lw   	$t0, 0($sp)   	# pop digit (t0) from stack
	addi 	$sp, $sp, 4  	# restore stack
	bltz 	$t0, negtve   	# if t0 <= 0 goto negtve
	j    	pop           	# else goto pop

negtve:	li   	$t0, '0'	# t0 = '0' (as a char)
	sb   	$t0, 0($a1)    	# *str = '0'
	addi 	$a1, $a1, 1  	# str++
	j    	next2      	

pop:	bltz 	$t0, next2     	# if t0 <= 0 goto next2
	addi 	$t0, $t0, '0'  	# else, t0 = char value of digit
	sb   	$t0, 0($a1)     	# *str = char value
	addi 	$a1, $a1, 1   	# str++
	lw   	$t0, 0($sp)   	# t0 = popped from stack
	addi 	$sp, $sp, 4   	# restore stack
	j    	pop           

next2:	sb  	$zero, 0($a1)  	# *str = 0 to signify end of string
	addi 	$sp, $sp, 4 	# restore stack
	lw   	$t0, 0($sp)  	# restore $t0 value
	jr  	$ra   	
	
nrbytes:# finds number of bytes to print, stored in $a2
	# a0 = integer
	li	$a2, 1		# a0 = 1 (minimum)
	li	$t0, 9
	bgt 	$a0, $t0, b3	# if a0 > 9, require 2 bytes min
	jr	$ra
b3:	addiu	$a2, $a2, 1
	li	$t0, 99
	bgt 	$a0, $t0, b4	# if a0 > 99, require 3 bytes min
	jr	$ra
b4:	addiu	$a2, $a2, 1
	li	$t0, 999
	bgt 	$a0, $t0, b5	# if a0 > 999, require 4 bytes min
	jr	$ra
b5:	addiu	$a2, $a2, 1
	li	$t0, 9999
	bgt 	$a0, $t0, b6	# if a0 > 9999, require 5 bytes min
	jr	$ra
b6:	addiu	$a2, $a2, 1
	li	$t0, 99999
	bgt 	$a0, $t0, b7	# if a0 > 99999, require 6 bytes min
	jr	$ra
b7:	addiu	$a2, $a2, 1
	li	$t0, 999999
	bgt 	$a0, $t0, b8	# if a0 > 999999, require 7 bytes min
	jr	$ra
b8:	addiu	$a2, $a2, 1
	li	$t0, 9999999
	bgt 	$a0, $t0, b9	# if a0 > 9999999, require 8 bytes min
	jr	$ra
b9:	addiu	$a2, $a2, 1
	li	$t0, 9999999
	bgt 	$a0, $t0, b10	# if a0 > 9999999, require 9 bytes
	jr	$ra
b10:	addiu	$a2, $a2, 1
	jr 	$ra
	
printl:	# prints newline
	li 	$v0, 15 	# syscall for writing to file
	move 	$a0, $s5 	# s5 = file descriptor	
	la	$a1, newline	# a1 = addr of newline
	li	$a2, 1		# write amount is 1 byte
	syscall 		# write to file
	beq 	$v0, -1, err  	# file writing error
	jr	$ra
	
prints:	# prints whitespace
	li 	$v0, 15 	# syscall for writing to file
	move 	$a0, $s5 	# s5 = file descriptor	
	la	$a1, whitespace	# a1 = addr of whitespace
	li	$a2, 1		# write amount is 1 byte
	syscall 		# write to file
	beq 	$v0, -1, err  	# file writing error
	jr	$ra

err:	# all types of errors are abstracted
	li 	$v0, 4 
	la 	$a0, errInvalidFile
	syscall 
	li 	$v0, 10		# exit...
	syscall
